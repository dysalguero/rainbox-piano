//constant variables
const keys = document.querySelectorAll(".key"),
  note = document.querySelector(".nowplaying"),
  hints = document.querySelectorAll(".hints1");
//end of const

//for audio play and note
function playNote(e) {
  const audio = document.querySelector(`audio[data-key="${e.keyCode}"]`),
    key = document.querySelector(`.key[data-key="${e.keyCode}"]`);

  if (!key) return;

  const keyNote = key.getAttribute("data-note");

  key.classList.add("playing");
  key.classList.toggle(`key-${e.keyCode}`); //toggle of colors
  note.innerHTML = keyNote;
  audio.currentTime = 0;
  audio.play();
}
window.addEventListener("keydown", playNote);
//end of function

function removeTransition(e) {
  if (e.propertyName !== "transform") return;
  this.classList.remove("playing");
}
keys.forEach(key => key.addEventListener("transitionend", removeTransition));

//for transition delay effects in hint
function hintsOn(e, index) {
  e.setAttribute("style", "transition-delay:" + index * 50 + "ms");
}
hints.forEach(hintsOn);
//end of function


